/// <reference path="../../typings/tsd.d.ts" />
import {XYZ, Vector3} from "./Vector3";

export class Quaternion {
  x: number;
  y: number;
  z: number;
  w: number;

  constructor(x: number, y: number, z: number, w: number) {
    this.x = (x) ? x : 0;
    this.y = (y) ? y : 0;
    this.z = (z) ? z : 0;
    this.w = (w) ? w : 1;
  }

  toString() { return `{ x: ${this.x}, y: ${this.y}, z: ${this.z}, w: ${this.w} }`; }

  set(x: number, y: number, z: number, w: number) { this.x = x; this.y = y; this.z = z; this.w = w; return this; }
  copy(q: Quaternion) { this.x = q.x; this.y = q.y; this.z = q.z; this.w = q.w; return this; }
  clone() { return new Quaternion(this.x, this.y, this.z, this.w); }

  setFromAxisAngle(axis: XYZ, angle: number) {
    let s = Math.sin(angle / 2);

    this.x = axis.x * s;
    this.y = axis.y * s;
    this.z = axis.z * s;
    this.w = Math.cos(angle / 2);
    return this;
  }

  setFromYawPitchRoll(yaw: number, pitch: number, roll: number) {
    let c1 = Math.cos(pitch / 2);
    let c2 = Math.cos(yaw / 2);
    let c3 = Math.cos(roll / 2);
    let s1 = Math.sin(pitch / 2);
    let s2 = Math.sin(yaw / 2);
    let s3 = Math.sin(roll / 2);

    this.x = s1 * c2 * c3 + c1 * s2 * s3;
    this.y = c1 * s2 * c3 - s1 * c2 * s3;
    this.z = c1 * c2 * s3 - s1 * s2 * c3;
    this.w = c1 * c2 * c3 + s1 * s2 * s3;
    return this;
  }

  multiplyQuaternions(a: Quaternion, b: Quaternion) {
    let qax = a.x;
    let qay = a.y;
    let qaz = a.z;
    let qaw = a.w;

    let qbx = b.x;
    let qby = b.y;
    let qbz = b.z;
    let qbw = b.w;

    this.x = qax * qbw + qaw * qbx + qay * qbz - qaz * qby;
    this.y = qay * qbw + qaw * qby + qaz * qbx - qax * qbz;
    this.z = qaz * qbw + qaw * qbz + qax * qby - qay * qbx;
    this.w = qaw * qbw - qax * qbx - qay * qby - qaz * qbz;

    return this;
  }

  multiply(q: Quaternion) { return this.multiplyQuaternions(this, q); }

  slerp(q: Quaternion, t: number) {
    // Adapted from Three.js
    if (t === 0) return this;
    if (t === 1) return this.copy(q);

    let x = this.x;
    let y = this.y;
    let z = this.z;
    let w = this.w;

    let cosHalfTheta = w * q.w + x * q.x + y * q.y + z * q.z;

    if (cosHalfTheta < 0) {
      this.w = -q.w;
      this.x = -q.x;
      this.y = -q.y;
      this.z = -q.z;
      cosHalfTheta = -cosHalfTheta;
    } else {
      this.copy(q);
    }

    if (cosHalfTheta >= 1.0) {
      this.w = w;
      this.x = x;
      this.y = y;
      this.z = z;
      return this;
    }

    let halfTheta = Math.acos(cosHalfTheta);
    let sinHalfTheta = Math.sqrt(1.0 - cosHalfTheta * cosHalfTheta);

    if (Math.abs(sinHalfTheta) < 0.001) {
      this.w = 0.5 * (w + this.w);
      this.x = 0.5 * (x + this.x);
      this.y = 0.5 * (y + this.y);
      this.z = 0.5 * (z + this.z);
      return this;
    }

    let ratioA = Math.sin((1 - t) * halfTheta) / sinHalfTheta,
    ratioB = Math.sin(t * halfTheta) / sinHalfTheta;

    this.w = (w * ratioA + this.w * ratioB);
    this.x = (x * ratioA + this.x * ratioB);
    this.y = (y * ratioA + this.y * ratioB);
    this.z = (z * ratioA + this.z * ratioB);
    return this;
  }
}
