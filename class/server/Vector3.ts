/// <reference path="../../typings/tsd.d.ts" />
import {Quaternion} from "./Quaternion";

export interface XYZ { x: number; y: number; z: number; }

export class Vector3 {
  static forward() { return new Vector3( 0,  0,  1); }
  static back()    { return new Vector3( 0,  0, -1); }
  static right()   { return new Vector3( 1,  0,  0); }
  static left()    { return new Vector3(-1,  0,  0); }
  static up()      { return new Vector3( 0,  1,  0); }
  static down()    { return new Vector3( 0, -1,  0); }

  x: number;
  y: number;
  z: number;

  constructor(x?: number, y?: number, z?: number) {
    if (x == null) x = 0;
    this.set(x, y, z);
  }

  toString() { return `{ x: ${this.x}, y: ${this.y}, z: ${this.z} }`; }

  set(x: number, y?: number, z?: number) {
    this.x = x;
    this.y = (y != null) ? y : x;
    this.z = (z != null) ? z : x;
    return this;
  }
  copy(v: XYZ) { this.x = v.x; this.y = v.y; this.z = v.z; return this; }
  clone() { return new Vector3(this.x, this.y, this.z); }

  add(x: number|XYZ, y?: number, z?: number) {
    if (y == null && z == null) {
      let v = <XYZ>x;
      this.x += v.x; this.y += v.y; this.z += v.z;
    } else {
      this.x += <number>x; this.y += y; this.z += z;
    }
    return this;
  }
  subtract(x: number|XYZ, y?: number, z?: number) {
    if (y == null && z == null) {
      let v = <XYZ>x;
      this.x -= v.x; this.y -= v.y; this.z -= v.z;
    } else {
      this.x -= <number>x; this.y -= y; this.z -= z;
    }
    return this;
  }
  multiplyScalar(m: number) { this.x *= m; this.y *= m; this.z *= m; return this; }

  cross(v: XYZ) {
    let x = this.x;
    let y = this.y;
    let z = this.z;

    this.x = y * v.z - z * v.y;
    this.y = z * v.x - x * v.z;
    this.z = x * v.y - y * v.x;
    return this;
  }

  dot(v: XYZ) { return this.x * v.x + this.y * v.y + this.z * v.z; }

  normalize() {
    let length = this.length();
    this.x /= length; this.y /= length; this.z /= length;
    return this;
  }
  negate() {
    this.x *= -1; this.y *= -1; this.z *= -1;
    return this;
  }

  lerp(v: XYZ, t: number) {
    this.x = this.x * (1 - t) + v.x * t;
    this.y = this.y * (1 - t) + v.y * t;
    this.z = this.z * (1 - t) + v.z * t;
    return this;
  }

  rotate(q: Quaternion) {
    let qx = q.x;
    let qy = q.y;
    let qz = q.z;
    let qw = q.w;

    let ix =  qw * this.x + qy * this.z - qz * this.y;
    let iy =  qw * this.y + qz * this.x - qx * this.z;
    let iz =  qw * this.z + qx * this.y - qy * this.x;
    let iw = - qx * this.x - qy * this.y - qz * this.z;

    this.x = ix * qw + iw * - qx + iy * - qz - iz * - qy;
    this.y = iy * qw + iw * - qy + iz * - qx - ix * - qz;
    this.z = iz * qw + iw * - qz + ix * - qy - iy * - qx;
    return this;
  }

  length() { return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z); }
  distanceTo(v: XYZ) {
    let x = this.x - v.x;
    let y = this.y - v.y;
    let z = this.z - v.z;
    return Math.sqrt(x*x + y*y + z*z);
  }
}
