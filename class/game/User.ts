/// <reference path="../../typings/tsd.d.ts" />
import {Bombe} from "./Bombe";
import {Map} from "./Map";
import {BlockType, CharacterAnim, Direction} from "./Structure";
import {Vector3} from "../server/Vector3";

export class User {
  private static io :SocketIO.Server;
  private name : string;

  private speed : number = 60 / 4;
  public speedTimer : number = 0;

  private bombePower : number = 2;

  public bombeNumber : number = 3;
  public bombeDropNumber : number = 0;

  public dead : boolean = true;
  public playing : boolean = false;
  public moving : boolean = false;
  public newOrient : boolean = false;
  public newAnim : boolean = false;

  public position : Vector3;
  public orientation : Vector3 = Vector3.forward();
  public animation : CharacterAnim = CharacterAnim.Idle;


  constructor( name: string){
    this.name = name;
  }

  public getName(){
    return this.name;
  }
  public toString(){
    return "Je m'appelle " + this.name  + ", je cours à " + this.speed +
     ", je peux posser " + this.bombeNumber + " bombe et j'ai " +
     this.bombePower + " Power !";
  }
  public static initIO(io : SocketIO.Server){
    User.io = io;
  }

  public getSpeed() :number{
    return this.speed;
  }

  public getBombePower():number{
    return this.bombePower;
  }

  public static sendInfoPlayer(socket : SocketIO.Socket, users : {}) :void{
    socket.on("sendInfoPlayer", function(name){
      users[socket.id] = new User(name);
      console.log("Nom : " + users[socket.id].getName());
    });
  }

  public static askAlivePlayers(socket : SocketIO.Socket, users : {}){
    socket.on("askAlivePlayers", function(info){
      let userInGame :{
        name : string,
        animation : CharacterAnim,
        position : {
          x:number,
          y:number,
          z:number
        },
        orientation : {
          x:number,
          y:number,
          z:number
        }
      }[] = [];
      Object.keys(User.io.sockets.adapter.rooms["Game"].sockets).forEach((userSocket) => {
        if(userSocket != socket.id && !users[userSocket].dead){
          let user = users[userSocket];
          userInGame.push({
            name : user.getName(),
             animation : user.animation,
             position : {x :user.position.x, y :user.position.y, z :user.position.z},
             orientation : { x :user.orientation.x, y :user.orientation.y, z :user.orientation.z }
           });
          }
      });
      //console.log(userInGame);
      if(userInGame.length > 0 ){
        socket.emit("sendAlivePlayer", userInGame);
      }
    });
  }

  public static askToPlay(socket : SocketIO.Socket, user : {}, userPlaying : {} , nbPlayer : number, spawnPointsToGive : any){
    socket.on("askToPlay", ()=>{
      let keys = Object.keys(userPlaying);
      if(keys.length < nbPlayer){
        if(!user[socket.id].playing){
          user[socket.id].playing = true;
          userPlaying[socket.id] = user[socket.id];
          let nbDead :number = 0;
          keys = Object.keys(userPlaying);
          keys.forEach((key) => {
            if(userPlaying[key].dead){
              nbDead++;
            }
          });
          console.log("nbDead : " + nbDead);
          if(nbDead == 2 && keys.length == 2){
            Map.sendSpawnPoint(spawnPointsToGive,userPlaying);
            console.log("start the game");
          }
        }
        else{
          console.log(user[socket.id].getName() + " allready playing");
        }
      }else{
        console.log("To much player : ask to play from " + user[socket.id].getName());
      }
    });
  }

  public static askMove(socket : SocketIO.Socket, mapObj : Map, users : {}, usersToMove : {}){
    socket.on("askMove", (direction : Direction) => {
      if(users[socket.id].playing && !users[socket.id].dead){
        let map : BlockType[][][] = mapObj.getMap();
        let user = users[socket.id];
        if(typeof direction === "number" && !user.moving){
          let userPos = user.position.clone();
          let endPosition = userPos;
          let newOrient : Vector3;
          switch(direction){
            case Direction.Forward :  endPosition.z -= 1; newOrient = Vector3.forward(); break;
            case Direction.Backward : endPosition.z += 1; newOrient = Vector3.back(); break;
            case Direction.Left :     endPosition.x -= 1; newOrient = Vector3.left(); break;
            case Direction.Right :    endPosition.x += 1; newOrient = Vector3.right(); break;
          }
          if(newOrient.z != user.orientation.z || newOrient.x != user.orientation.x){
            user.newOrient = true;
            user.orientation = newOrient;
          }
          let type = map[endPosition.x][Math.ceil(endPosition.y)][endPosition.z];
          if(type == BlockType.Void){
            user.moving = true;
            user.newAnim = true;
            user.animation = CharacterAnim.Walk;
            usersToMove[socket.id] = {user : user, startPosition : user.position.clone() ,endPosition : endPosition };
          }
        }
      }
    });
  }

  public static askDropBombe(socket : SocketIO.Socket, map : Map, users : {}, allBombe : Bombe[]){
    socket.on("askDropBombe", () => {
      if(users[socket.id].playing && !users[socket.id].dead){
        let user = users[socket.id];
        let mapping = map.getMap();
        let userPosition = user.position;
        let x = Math.round(userPosition.x);
        let y = Math.round(userPosition.y);
        let z = Math.round(userPosition.z);
        if(user.bombeDropNumber < user.bombeNumber && mapping[x][y][z] != BlockType.Bombe){
          let bombe = new Bombe(user, new Vector3(x,y,z));
          let position = bombe.position;
          User.io.sockets.to("Game").emit("initBombe", {
            id : bombe.getID(),
            position : {
              x : position.x,
              y : position.y-0.5,
              z : position.z,
            }
          });
          map.setBlockAt(position.x, position.y, position.z, BlockType.Bombe);
          allBombe.push(bombe);
          user.bombeDropNumber++;
        }
      }
    });
  }

  public static sendAllOrientation(users : {}){
    let usersPlaying : {name : string, orientation : {x :number, y:number, z:number}}[] = [];
    Object.keys(User.io.sockets.adapter.rooms["Game"].sockets).forEach((userSocket) => {
      let user = users[userSocket];
      if(user.playing && !user.dead && user.newOrient){
        let orientation = user.orientation;
        usersPlaying.push({
          name : user.getName(),
          orientation : {
              x : orientation.x,
              y : orientation.y,
              z : orientation.z
          }
        });
        user.newOrient = false;
      }
    });
    if(usersPlaying.length > 0 ){
      User.io.sockets.to("Game").emit("sendAllOrientation", usersPlaying);
    }
  }

  public static sendAllPosition(users : {}){
    let usersPlaying : {name : string, position : {x :number, y:number, z:number}}[] = [];
    Object.keys(User.io.sockets.adapter.rooms["Game"].sockets).forEach((userSocket) => {
      let user = users[userSocket];
      if(user.playing && !user.dead && user.moving){
        let user = users[userSocket]
        let position = user.position
        usersPlaying.push({
          name : user.getName(),
          position : {
              x : position.x,
              y : position.y,
              z : position.z
          }
        });
      }
    });
    if(usersPlaying.length > 0 ){
      User.io.sockets.to("Game").emit("sendAllPosition", usersPlaying);
    }
  }

  public static sendAllAnimation(users : {}){
    let usersPlaying : {name : string, animation : CharacterAnim}[] = [];
    Object.keys(User.io.sockets.adapter.rooms["Game"].sockets).forEach((userSocket) => {
      let user = users[userSocket];
      if(user.playing && !user.dead && user.newAnim){
        //user.animation = CharacterAnim.Walk;
        usersPlaying.push({
          name : user.getName(),
          animation :user.animation
        });
        user.newAnim = false;
      }
    });
    if(usersPlaying.length > 0 ){
      User.io.sockets.to("Game").emit("sendAllAnimation", usersPlaying);
    }
  }
}
