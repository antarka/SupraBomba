/// <reference path="../../typings/tsd.d.ts" />
import {SpawnType} from "./Structure";
import {Vector3} from "../server/Vector3";

export class SpawnPoint{
  private type : SpawnType;
  private position : Vector3;

  constructor(type :SpawnType, x:number, y :number, z :number){
    this.type = type;
    this.position = new Vector3(x,y,z);
  }

  public getPosition() :Vector3 {
    return this.position.clone();
  }

}
