/// <reference path="../../typings/tsd.d.ts" />
import {User} from "./User";
import {Map} from "./Map";
import {Vector3} from "../server/Vector3";
import {BlockType} from "./Structure";

export class Bombe{
  private ID : number;
  private parent : User;
  public timer : number = 0;
  public duration : number = 3 * 60;
  public position : Vector3;
  public inExplosion : boolean = false;

  private static io : SocketIO.Server;
  private static count : number = 1
  private static bombes : Bombe[] = [];

  constructor(user : User, position : Vector3){
    this.parent  = user;
    this.position = position;//user.position.clone();
    this.ID = Bombe.count;
    Bombe.count++;
    Bombe.bombes.push(this);
  }

  destroy(){
    let index = Bombe.bombes.indexOf(this);
    if(index != -1){
      Bombe.bombes.splice(index,1);
    }
  }

  public static initIO(io: SocketIO.Server){
    Bombe.io = io;
  }

  public getID() :number{
    return this.ID;
  }

  public getUser() :User{
    return this.parent;
  }

  public static getBombeAt(x: number, y :number, z :number) :Bombe{
    let theBombe : Bombe;
    for(let i = 0; i < Bombe.bombes.length; i++){
      let position = Bombe.bombes[i].position;
      if(position.x == x && position.y == y && position.z == z){
        theBombe = Bombe.bombes[i];
        break;
      }
    }
    return theBombe;
  }

  public explosion(mapObj :Map, width :number, depth :number, bombeExplosion : Bombe[], userPlaying : {}){
    this.inExplosion = true;
    let bombePower = this.parent.getBombePower();
    let map = mapObj.getMap();
    let y = Math.ceil(this.position.y);
    let actualizeBlock : {type : BlockType, position : {x:number, y:number, z:number}}[] = [];
    let powerRight : number = 0;
    let powerLeft : number = 0;
    let powerUp : number = 0;
    let powerDown : number = 0;
    for(let i = 0; i < 4 ; i++){
      for(let j = 0; j < bombePower; j++){
        let position = this.position.clone();
        switch(i){
          case 0: position.x += (j+1); powerRight++; break;
          case 1: position.x -= (j+1); powerLeft++; break;
          case 2: position.z -= (j+1); powerUp++; break;
          case 3: position.z += (j+1); powerDown++; break;

        }
        if(position.x > width || position.x < 0 || position.z < 0 || position.z > depth) break;
        let type = map[position.x][y][position.z]
        if(type == BlockType.Crate){
          map[position.x][y][position.z] = BlockType.Void
          actualizeBlock.push({
            type : BlockType.Void,
            position : {
              x : position.x,
              y : y,
              z : position.z
            }
          });
          break;
        }
        else if(type == BlockType.Bombe){
          let bombe = Bombe.getBombeAt(position.x, position.y, position.z);
          if(!bombe.inExplosion){
            bombeExplosion.push(bombe);
            bombe.inExplosion = true;
            //console.log("bombe Detected !");
          }
          break;
        }
        else if(type == BlockType.Wall){
          switch(i){
            case 0: powerRight--; break;
            case 1: powerLeft--; break;
            case 2: powerUp--; break;
            case 3: powerDown--; break;
          }
          break;
        }

      }
    }
    //console.log("----------");
    this.parent.bombeDropNumber--;
    if(actualizeBlock.length > 0){
      Bombe.io.sockets.to("Game").emit("actualizeMap", actualizeBlock);
    }
    mapObj.setBlockAt(this.position.x, this.position.y, this.position.z, BlockType.Void);
    Bombe.io.sockets.to("Game").emit("destroyBombe", {
      id : this.ID,
      powerRight: powerRight,
      powerLeft : powerLeft,
      powerUp : powerUp,
      powerDown : powerDown
    });
    this.checkPlayerInExplosion( powerRight, powerLeft, powerUp, powerDown, userPlaying );
    this.destroy();
  }

  private checkPlayerInExplosion( powerRight:number, powerLeft:number, powerUp:number, powerDown:number, userPlaying : {}){
    let keys = Object.keys(userPlaying);
    /*console.log("--------------------");
    console.log("powerRight : " + powerRight);
    console.log("powerLeft : " + powerLeft);
    console.log("powerUp : " + powerUp);
    console.log("powerDown : " + powerDown);*/

    keys.forEach((key) => {
      let user = userPlaying[key];
      let userPosition : {x:number, y:number, z:number} = user.position;
      let bombePosition : {x:number, y:number, z:number} = this.position;
      let offsetX : number = bombePosition.x - userPosition.x;
      let offsetY : number = bombePosition.z - userPosition.z;
      let dieUser : boolean = false;
      /*
      console.log("check for user : " + user.getName());
      console.log("userPosition : " + userPosition);
      console.log("bombePosition : " + bombePosition);
      console.log("offsetX : " + offsetX);
      console.log("offsetY : " + offsetY);*/

      if( Math.abs(offsetX) <= 0.5 && Math.abs(offsetY) <= 0.5 )
        dieUser = true;
      // si le user est a droite de la bombe
      else if(offsetX < -0.5){
        if( Math.abs(offsetX) <= powerRight + 0.5 && Math.abs(offsetY) <= 0.5 )
          dieUser = true;
      }
      // si le user est a gauche de la bombe
      else if(offsetX > 0.5){
        if( Math.abs(offsetX) <= powerLeft + 0.5 && Math.abs(offsetY) <= 0.5 )
          dieUser = true;
      }
      // si le user est en bas de la bombe
      else if(offsetY < -0.5){
        if( Math.abs(offsetY) <= powerDown + 0.5 && Math.abs(offsetX) <= 0.5 )
          dieUser = true;
      }
      // si le user est en haut de la bombe
      else if(offsetY > 0.5){
        if( Math.abs(offsetY) <= powerUp + 0.5 && Math.abs(offsetX) <= 0.5 )
          dieUser = true;
      }
      if(dieUser){
        user.dead = true;
        console.log("user ["+user.getName()+"] hited by bombe");
      }
      //console.log("--------------------");
    });


  }
}
