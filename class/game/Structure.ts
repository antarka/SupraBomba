/// <reference path="../../typings/tsd.d.ts" />

export enum BlockType{
  Void,
  Floor,
  Wall,
  Crate,
  Bombe
};

export enum SpawnType{
  FFA,
  TEAM_BLUE,
  TEAM_RED,
  FFA_TEAM_BLUE,
  FFA_TEAM_RED
}

export enum GameType{
    FFA,
    TEAM,
    TEAM_FFA
}

export enum GameState{
    Warmup,
    Playing,
    Pause
}

export enum CharacterAnim{
  Idle,
  Walk
}

export enum Direction{
  Forward,
  Backward,
  Left,
  Right
}
