/// <reference path="../../typings/tsd.d.ts" />
import {SpawnPoint} from "./SpawnPoint";
import {Vector3} from "../server/Vector3";
import {BlockType, SpawnType, GameType, GameState} from "./Structure";
import {User} from "./User";

export class Map{
  private static io : SocketIO.Server;

  private width :number;
  private depth :number;
  private height :number;

  private gameType :GameType;
  private spawnPoints : SpawnPoint[] = [];
  private spawnPointsFFA : {x:number, y:number, z:number}[];
  private spawnPointsBlueTeam : {x:number, y:number, z:number}[];
  private spawnPointsRedTeam : {x:number, y:number, z:number}[];

  private map : BlockType[][][] = [];
  private defaultMap : BlockType[][][] = [];

  constructor(jsonMap : any){
    this.width = jsonMap.width;
    this.height = jsonMap.nbLayer;
    this.depth = jsonMap.height;
    this.gameType = jsonMap.game;

    //this.map = []
    for(let x = 0; x < this.width; x++){
      this.defaultMap[x] = [];
        this.map[x] = [];
      for(let y = 0; y < this.height; y++ ){
        this.defaultMap[x][y] = [];
          this.map[x][y] = [];
        for(let z = 0; z < this.depth; z++){
          this.defaultMap[x][y][z] = BlockType.Void;
            this.map[x][y][z] = BlockType.Void;
        }
      }
    }

    for(let i = 0; i < jsonMap.Cells.length; i++){
      let position = jsonMap.Cells[i].position
      let type = jsonMap.Cells[i].type;
      this.defaultMap[position.x][position.y][position.z] = type;
      this.map[position.x][position.y][position.z] = type;
    }
    //console.log(this.defaultMap);

    // Init SpawnPoints Array;
    if(this.gameType == GameType.FFA || this.gameType == GameType.TEAM_FFA){
      this.spawnPointsFFA = [];
    }
    if(this.gameType == GameType.TEAM || this.gameType == GameType.TEAM_FFA){
      this.spawnPointsRedTeam = [];
      this.spawnPointsBlueTeam = [];
    }

    // Sort spawnPoints by properties
    for(let i = 0; i < jsonMap.SpawnPoints.length; i++){
      let position = jsonMap.SpawnPoints[i].position
      let type = jsonMap.SpawnPoints[i].type;
      this.spawnPoints[i] = new SpawnPoint(type,position.x, position.y, position.z);
      if( type == SpawnType.FFA || type == SpawnType.FFA_TEAM_BLUE || type == SpawnType.FFA_TEAM_RED){
        this.spawnPointsFFA.push(this.spawnPoints[i].getPosition());
      }
      if( type == SpawnType.FFA_TEAM_BLUE ||  type == SpawnType.TEAM_BLUE){
        this.spawnPointsBlueTeam.push(this.spawnPoints[i].getPosition());
      }
      if( type == SpawnType.FFA_TEAM_RED ||  type == SpawnType.TEAM_RED){
        this.spawnPointsRedTeam.push(this.spawnPoints[i].getPosition());
      }
    }
  }

  public static initIO(io : SocketIO.Server){
    Map.io = io;
  }
  public getDefaultMap(){
    return this.defaultMap;
  }

  public getMap(){
    return this.map;
  }

  public getWidth() :number{
    return this.width;
  }
  public getHeight() :number{
    return this.height;
  }
  public getDepth() :number{
    return this.depth;
  }

  public resetMap() :void{
    this.cloneMap();
    //this.map = this.defaultMap.slice( 0 );
    //console.log("map : ");
    //console.log(this.map);
  }

  private cloneMap() : any {
    this.map = JSON.parse(JSON.stringify(this.defaultMap));
  }

  public getBlockAt(x:number, y:number, z:number) :BlockType{
    return this.map[x][Math.ceil(y)][z];
  }
  public setBlockAt(x:number, y:number, z:number, type : BlockType) :void{
    this.map[x][Math.ceil(y)][z] = type;

  }
  public getSpawnPoint( gameMode :GameType)
  :{x:number, y:number, z:number}[] | {blue : {x:number, y:number, z:number}[], red :{x:number, y:number, z:number}[]} |
  {ffa :{x:number, y:number, z:number}[], blue :{x:number, y :number, z:number}[], red :{x:number, y:number, z:number}[]} {
    if(gameMode == GameType.FFA){
      if(this.gameType == GameType.TEAM_FFA || this.gameType == GameType.FFA){
        return this.spawnPointsFFA;
      }
      else{
        console.log("This map is not playable in FFA mode !");
      }
    }
    else if(gameMode == GameType.TEAM){
      if(this.gameType == GameType.TEAM_FFA || this.gameType == GameType.TEAM){
        return {blue :this.spawnPointsBlueTeam, red :this.spawnPointsRedTeam};
      }
      else{
        console.log("This map is not playable in TEAM mode !");
      }
    }
    else if(gameMode == GameType.TEAM){
      if(this.gameType == GameType.TEAM_FFA){
        return {ffa :this.spawnPointsFFA, blue :this.spawnPointsBlueTeam, red :this.spawnPointsRedTeam};
      }
      else{
        if(this.gameType == GameType.FFA){
          console.log("This map is not playable in TEAM mode !");
        }
        else if(this.gameType == GameType.TEAM){
          console.log("This map is not playable in FFA mode !");
        }
      }
    }
  }

  public askServerMap(socket : SocketIO.Socket){
    socket.on("askServerMap", () => {
      /*for(let y = 0; y < this.height; y++ ){
        for(let x = 0; x < this.width; x++){
          let str : string = "[ ";
          for(let z = 0; z < this.depth; z++){
            if (z == this.depth -1 ){
              str += this.defaultMap[x][y][z];
            }
            else{
              str += this.defaultMap[x][y][z] + " ";
            }
          }
          str += " ]"
          console.log(str);
        }
        console.log("");
      }*/
      socket.emit("sendMapToClient", { current : this.map, default : this.defaultMap });
      socket.join("Game");
    });
  }

  public static sendSpawnPoint(allspawnPoints : any, userPlaying : {}){
    let keys = Object.keys(userPlaying);
    let spawnPoints = allspawnPoints.slice(0);
    keys.forEach((key) => {
      let user = userPlaying[key];
      let playing = user.playing;
      //console.log("");
      if(playing /*&& user.dead*/){
        let random = Math.floor(Math.random() * spawnPoints.length);
        let position = spawnPoints[random];
        //user.playing = true;
        user.dead = false;
        user.position = new Vector3(position.x, position.y, position.z);
        Map.io.to(key).emit("sendSpawnPoint", {
          spawnPoint : spawnPoints[random],
          name : user.getName(),
          orientation : { x :user.orientation.x, y :user.orientation.y, z :user.orientation.z },
        });
        //console.log("key : " + key );
        //console.log(Map.io.sockets.sockets[key]);
        Map.io.sockets.sockets[key].to("Game").broadcast.emit("initNewPlayer", {
          name : user.getName(),
          position : { x :position.x, y :position.y, z :position.z },
          orientation : { x :user.orientation.x, y :user.orientation.y, z :user.orientation.z },
        });
        //console.log("spawn");
        spawnPoints.splice(random,1);
      }
      else{
        console.log("Le client est déja en jeu !");
      }
    });
  }
}
