/// <reference path="typings/tsd.d.ts" />
/// <reference path="class/game/User.ts" />
import * as IO from "socket.io";
import * as fs from "fs";
let io = IO.listen(8082);
import {XYZ, Vector3} from "./class/server/Vector3";
import {Quaternion} from "./class/server/Quaternion";
import {User} from "./class/game/User";
import {Map} from "./class/game/Map";
import {Bombe} from "./class/game/Bombe";
//import {SpawnPoint} from "./class/game/SpawnPoint";
//import {Block} from "./class/game/Block";
import {BlockType, SpawnType, GameType, GameState, CharacterAnim} from "./class/game/Structure";

let mapJSON = require("./maps/first_map.json");
let map : Map = new Map(mapJSON);

Bombe.initIO(io);
User.initIO(io);
Map.initIO(io);

console.log('Server  sur écoute 8082 !');


let gameMode = GameType.FFA;
let gameState = GameState.Warmup;
let spawnPointsToGive : any = map.getSpawnPoint(gameMode);
//console.log("spawnPointsToGive : " + spawnPointsToGive);

let userPlaying : {} = {};

let users : {} = {};
let usersToMove : {} = {};
let allBombes : Bombe[] = [];
let bombeExplosion : Bombe[] = [];

let gameLoop = setInterval(function(){
  if(Object.keys(users).length > 0){
    let keys = Object.keys(usersToMove);
    if( keys.length != 0){
      for(let i = 0 ; i < keys.length; i++){
        let user = usersToMove[keys[i]].user;
        if(!user.moving){
          user.animation = CharacterAnim.Idle;
          user.newAnim = true;
          delete usersToMove[keys[i]];
        }
        else{
          user.speedTimer++;
          let speedTimer = user.speedTimer;
          let speedUser = user.getSpeed();

          let startPos = usersToMove[keys[i]].startPosition;
          let endPos = usersToMove[keys[i]].endPosition;
          let factor = speedTimer / speedUser;
          let newPos = startPos.clone().lerp(endPos, factor);
          user.position = new Vector3(newPos.x, newPos.y, newPos.z);
          if(speedTimer >= speedUser){
            user.moving = false;
            user.speedTimer = 0;
          }
        }
      }
      User.sendAllOrientation(users);
      User.sendAllPosition(users);
      User.sendAllAnimation(users);
    }

    if(bombeExplosion.length > 0 ){
      let bombeExplosionLength = bombeExplosion.length;
      for(let i = bombeExplosionLength-1 ; i > -1 ; i--){
        let user = bombeExplosion[i].getUser();
        bombeExplosion[i].explosion(map, map.getWidth(), map.getDepth(), bombeExplosion, userPlaying);
        for(let j = 0 ; j < allBombes.length; j++){
          if(bombeExplosion[i].getID() == allBombes[j].getID()){
            allBombes.splice(j,1);
            break;
          }
        }
          bombeExplosion.splice(i, 1);
      }
    }
    if( allBombes.length != 0){
      let removeIndex : number[] = [];
      for(let i = 0 ; i < allBombes.length; i++){
          let bombe = allBombes[i];
          if(bombe.timer >= bombe.duration){
            let user = bombe.getUser();
            removeIndex.push(i);
            bombe.explosion(map, map.getWidth(), map.getDepth(), bombeExplosion, userPlaying);
          }
          else{
            bombe.timer++;
          }
      }
      for(let i = removeIndex.length-1 ; i > -1; i--){
        allBombes.splice(removeIndex[i],1);
      }
    }
  }
  if(Object.keys(userPlaying).length >= 2){
    let nbAlive : number = 0;
    let keys = Object.keys(userPlaying);
    keys.forEach((key) => {
      if(!userPlaying[key].dead){
        nbAlive++;
      }
    });
    //console.log("player alive : " + nbAlive);
    if(nbAlive < 2){
      resetRound();
      io.sockets.to("Game").emit("resetMap");
      Map.sendSpawnPoint(spawnPointsToGive, userPlaying);
    }
  }
},1000/60);


function resetRound(){
    spawnPointsToGive = map.getSpawnPoint(gameMode);
    resetMap();
}

function resetMap(){
  map.resetMap();
  console.log()
}

io.on('connection', function (socket) {
  console.log("un client vient de se connecter ! ");

  User.sendInfoPlayer(socket, users);
  map.askServerMap(socket);
  User.askAlivePlayers(socket, users);

  User.askToPlay(socket, users, userPlaying, mapJSON.nbPlayer, spawnPointsToGive);

  //map.askSpawnPoint(socket, spawnPointsToGive, users);
  User.askMove(socket, map, users, usersToMove);
  User.askDropBombe(socket, map, users, allBombes);



  socket.on('disconnect', function(){
    if(users[socket.id]){
      console.log( users[socket.id].getName() + " vient de se déconnecter ! ");
      io.sockets.to("Game").emit("userDisconnected", users[socket.id].getName());

    }
    else{
      console.log("Un client s'est déconnecter ! ");
    }
    delete users[socket.id];
    delete userPlaying[socket.id];
    if(Object.keys(users).length == 0 ){
      spawnPointsToGive = map.getSpawnPoint(gameMode);
    }
  });
});
